import pretty_print.{
  append, append_t, break, break_after, break_before, empty, group, indent, line,
  lines, render, sequence, sequence_trailing, text,
}
import gleam/should
import gleam/list

pub fn empty_test() {
  empty()
  |> render(0)
  |> should.equal("")
}

pub fn text_test() {
  text("hello")
  |> render(80)
  |> should.equal("hello")
}

pub fn append_test() {
  let doc =
    text("left")
    |> append(text("-"))
    |> append(text("right"))

  render(doc, 100)
  |> should.equal("left-right")

  render(doc, 5)
  |> should.equal("left-right")
}

pub fn append_t_test() {
  let doc =
    text("left")
    |> append_t("-")
    |> append_t("right")

  render(doc, 100)
  |> should.equal("left-right")

  render(doc, 5)
  |> should.equal("left-right")
}

pub fn group_test() {
  text("left")
  |> append_t("-right")
  |> group()
  |> render(1)
  |> should.equal("left-right")

  text("left")
  |> append(break(""))
  |> append_t("-right")
  |> group()
  |> render(1)
  |> should.equal("left\n-right")
}

pub fn break_test() {
  let doc =
    text("left")
    |> append(break(","))
    |> append_t("right")

  doc
  |> render(100)
  |> should.equal("left,right")

  doc
  |> render(3)
  |> should.equal("left,\nright")
}

pub fn indent_test() {
  text("left")
  |> append(break(""))
  |> append_t("-middle")
  |> append(break(""))
  |> append_t("-right")
  |> group()
  |> indent(2)
  |> render(1)
  |> should.equal("left\n  -middle\n  -right")
}

pub fn line_test() {
  text("top")
  |> append(line())
  |> append_t("bottom")
  |> render(100)
  |> should.equal("top
bottom")
}

pub fn lines_test() {
  text("top")
  |> append(lines(3))
  |> append_t("bottom")
  |> render(100)
  |> should.equal("top


bottom")
}

pub fn break_before_test() {
  let doc =
    text("one")
    |> append(break_before(" | ", "| "))
    |> append_t("two")
    |> append(break_before(" | ", "| "))
    |> append(text("three"))
    |> group()

  doc
  |> render(100)
  |> should.equal("one | two | three")

  doc
  |> render(8)
  |> should.equal("one
| two
| three")
}

pub fn break_after_test() {
  let doc =
    text("one")
    |> append(break_after(" | ", " |"))
    |> append_t("two")
    |> append(break_after(" | ", " |"))
    |> append(text("three"))

  doc
  |> render(100)
  |> should.equal("one | two | three")

  doc
  |> render(8)
  |> should.equal("one |
two |
three")
}

pub fn two_words_test() {
  text("hello")
  |> append(break_after(",", ","))
  |> group()
  |> append(text("there"))
  |> render(8)
  |> should.equal("hello,\nthere")
}

pub fn two_words_indent_test() {
  text("hello")
  |> append(break_after(",", ","))
  |> group()
  |> append(text("there"))
  |> indent(2)
  |> render(8)
  |> should.equal("hello,\n  there")
}

pub fn source_code_test() {
  text("pub fn(")
  |> append(
    group(
      break("")
      |> append(
        text("a")
        |> append_t(": ")
        |> append_t("String"),
      )
      |> append_t(",")
      |> append(break(""))
      |> append(
        text("b")
        |> append_t(": ")
        |> append_t("Int")
        |> append_t(","),
      ),
    )
    |> indent(2),
  )
  |> append(break(""))
  |> append_t(")")
  |> append_t(" -> ")
  |> append_t("String")
  |> render(15)
  |> should.equal("pub fn(
  a: String,
  b: Int,
) -> String")
}

pub fn sequence_test() {
  let left = text("( ")
  let items =
    ["one", "two", "three", "four"]
    |> list.map(text)
  let sep = break_before(", ", ", ")
  let right = break_before(" )", ")")
  let seq = sequence(left, items, sep, right, 2)

  seq
  |> render(100)
  |> should.equal("( one, two, three, four )")

  seq
  |> render(10)
  |> should.equal("( one
  , two
  , three
  , four
)")
}

pub fn sequence_trailing_test() {
  let left = append(text("("), break_after(" ", ""))
  let items =
    ["one", "two", "three", "four"]
    |> list.map(text)
  let sep = break_after(", ", ",")
  let right = text(")")
  let seq = sequence_trailing(left, items, sep, right, 2)

  seq
  |> render(100)
  |> should.equal("( one, two, three, four, )")

  seq
  |> render(10)
  |> should.equal("(
  one,
  two,
  three,
  four,
)")
}
