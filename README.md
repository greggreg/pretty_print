# Pretty Print

#### A Gleam library for formatting documents like source code.

See the documentation for many examples:
[https://hexdocs.pm/pretty_print/](https://hexdocs.pm/pretty_print/)

Available in hex:
[https://hex.pm/packages/pretty_print](https://hex.pm/packages/pretty_print)

Render documents to String using the least number of lines possible while trying to stay under a certain number of columns.

Works well for constraining source code to a particular width.

## Examples

    // The same Document may pretty print as
    [ 1, 2, 3, 4 ]

    // or
    [ 1
    , 2
    , 3
    , 4
    ]
    // depending on the desired maximum columns.

    // Similarly
    one().two().three().four()

    // may break to
    one()
      .two()
      .three()
      .four()

The core concepts here are:

- Text nodes will never break.
- Break nodes will break if they must.
- Documents can be placed into groups.
- Groups may be inside of groups.
- Outer groups will always break before inner groups.
- Groups can be told to indent their children when broken.

This is essentially some convenience wrappers around this algorithm:
http://citeseerx.ist.psu.edu/viewdoc/summary?doi=10.1.1.34.2200

## Installation

This package can be installed by adding `pretty_print` to your `rebar.config` dependencies:

```erlang
{deps, [
    pretty_print
]}.
```
