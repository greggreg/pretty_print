//// A module for printing documents using the least number of lines possible
//// while trying to stay under a certain number of columns.
////
//// Works well for constraining source code to a particular width.
////
////
//// ## Examples
////
////    // The same Document may pretty print as
////    [ 1, 2, 3, 4 ]
////    
////    // or
////    [ 1
////    , 2
////    , 3
////    , 4
////    ]
////    // depending on the desired maximum columns.
////
////    // Similarly
////    one().two().three().four()
////    
////    // may break to
////    one()
////      .two()
////      .three()
////      .four()
////
////
//// The core concepts here are:
//// * Text nodes will never break.
//// * Break nodes will break if they must.
//// * Documents can be placed into groups.
//// * Groups may be inside of groups.
//// * Outer groups will always break before inner groups.
//// * Groups can be told to indent their children when broken.
////
//// This is essentially some convenince wrappers around this algorithm:
//// http://citeseerx.ist.psu.edu/viewdoc/summary?doi=10.1.1.34.2200
//// 

import gleam/list
import gleam/string
import gleam/string_builder

/// The core datastructure of a document being built. You cannot build
/// a document directly but you can use the functions in this module to
/// build one.
pub opaque type Document {
  // An empty document, nothing printed
  Empty
  // A text node, never broken
  Text(String)
  // A possible break point
  // if unbroken the left String is used as a joiner
  // if broken the right String is used
  OnBreak(String, BreakPosition)
  // Attach the right Document after the left Document
  Cons(Document, Document)
  // A grouping of documents, will be broken together if needed
  Group(Document)
  // How deeply to nest a group's children if it is broken
  Nest(Int, Document)
}

// Where to break when we must break
type BreakPosition {
  Before(String)
  After(String)
}

type SDoc {
  SNil
  SText(String, SDoc)
  SLine(Int, SDoc)
}

type Mode {
  Flat
  Break
}

/// Render a document to String.
///
/// Takes a Document and a number of column to try to fit it into.
///
/// See the other functions in this module for many examples.
pub fn render(document doc: Document, columns w: Int) -> String {
  let sdoc = format(w, 0, [tuple(0, Flat, Group(doc))])
  let str = sdoc_to_string(sdoc)
  str
}

/// An empty Document. Renders nothing. Useful when you need to conditionally
/// render something.
///
/// ## Examples
///
///    empty()
///    |> render(0)
///    |> should.equal("")
pub fn empty() -> Document {
  Empty
}

/// An exact string to render. Never gets broken up.
///
/// ## Examples
///
///    text("hello")
///    |> render(80)
///    |> should.equal("hello")
pub fn text(s: String) -> Document {
  Text(s)
}

/// Append one Document to Another. Does not indicate a break, simply joining
/// two documents together. append(text(), text()) will never get broken up.
///
/// ## Examples
///
///    let doc =
///      text("left")
///      |> append(text("-"))
///      |> append(text("right"))
///
///    render(doc, 100)
///    |> should.equal("left-right")
///
///    render(doc, 5)
///    |> should.equal("left-right")
pub fn append(l: Document, r: Document) -> Document {
  Cons(l, r)
}

/// Convenience function for appending a String to a document.
///
/// ## Examples
///
///    let doc =
///      text("left")
///      |> append_t("-")
///      |> append_t("right")
///
///    render(doc, 100)
///    |> should.equal("left-right")
///
///    render(doc, 5)
///    |> should.equal("left-right")
pub fn append_t(l: Document, r: String) -> Document {
  Cons(l, Text(r))
}

/// Groups represent a document hierarchy. When a document must be broken
/// outer groups always break before inner groups. Groups that do not contain
/// any `break()`s will never be broken.
///
/// ## Examples
///
///    text("left")
///    |> append_t("-right")
///    |> group()
///    |> render(1)
///    |> should.equal("left-right")
///
///    text("left")
///    |> append(break(""))
///    |> append_t("-right")
///    |> group()
///    |> render(1)
///    |> should.equal("left\n-right")
pub fn group(d: Document) -> Document {
  Group(d)
}

/// A possible break in the document.
/// The same string will be used whether the document breaks or not.
/// The break will always occur after the String.
/// 
/// ##  Examples
/// 
///     let doc =
///       text("left")
///       |> append(break(","))
///       |> append_t("right")
///   
///     doc
///     |> render(100)
///     |> should.equal("left,right")
///   
///     doc
///     |> render(3)
///     |> should.equal("left,
///     right")
pub fn break(s: String) -> Document {
  OnBreak(s, After(s))
}

/// A possible break in the Document that will occur after the String.
/// The first String is for when the Document doesn't break, and the
/// second is for when the Document does break.
///
/// ## Examples
/// 
///    let doc =
///      text("one")
///      |> append(break_after(" | ", " |"))
///      |> append_t("two")
///      |> append(break_after(" | ", " |"))
///      |> append(text("three"))
///    
///    doc
///    |> render(100)
///    |> should.equal("one | two | three")
///    
///    doc
///    |> render(8)
///    |> should.equal("one |
///    two |
///    three")
pub fn break_after(no_break: String, break: String) -> Document {
  OnBreak(no_break, After(break))
}

/// A possible break in the Document that will occur after the String.
/// The first String is for when the Document doesn't break, and the
/// second is for when the Document does break.
///
/// ## Examples
/// 
///    let doc =
///      text("one")
///      |> append(break_before(" | ", "| "))
///      |> append_t("two")
///      |> append(break_before(" | ", "| "))
///      |> append(text("three"))
///      |> group()
///    
///    doc
///    |> render(100)
///    |> should.equal("one | two | three")
///    
///    doc
///    |> render(8)
///    |> should.equal("one
///    | two
///    | three")
pub fn break_before(no_break: String, break: String) -> Document {
  OnBreak(no_break, Before(break))
}

/// Indicate how deeply to indent the children of a group when it must be broken.
///
/// ## Examples
///
///    text("left")
///    |> append(break(""))
///    |> append_t("-middle")
///    |> append(break(""))
///    |> append_t("-right")
///    |> group()
///    |> indent(2)
///    |> render(1)
///    |> should.equal("left\n  -middle\n  -right")
pub fn indent(document d: Document, depth i: Int) -> Document {
  Nest(i, d)
}

/// Forces a new line. A convenience method for `text("\n")`.
///
/// ## Examples
///
///    text("top")
///    |> append(line())
///    |> append_t("bottom")
///    |> render(100)
///    |> should.equal("top
///    bottom")
pub fn line() -> Document {
  text("\n")
}

/// Forces a number of new lines.
///
/// ## Examples
///
///    text("top")
///    |> append(lines(3))
///    |> append_t("bottom")
///    |> render(100)
///    |> should.equal("top
///    
///    
///    bottom")
pub fn lines(number n: Int) -> Document {
  case n {
    x if x > 0 -> append(line(), lines(n - 1))
    _ -> empty()
  }
}

/// Helper for grouping and nesting a sequence of items.
/// If you dont need an opening, closing, or separator you can set them
/// to `empty()`.
///
/// ## Examples
/// 
///    let left = text("( ")
///    let items =
///      ["one", "two", "three", "four"]
///      |> list.map(text)
///    let sep = break_before(", ", ", ")
///    let right = break_before(" )", ")")
///    let seq = sequence(left, items, sep, right, 2)
///
///    seq
///    |> render(100)
///    |> should.equal("( one, two, three, four )")
///
///    seq
///    |> render(10)
///    |> should.equal("( one
///    , two
///    , three
///    , four
///    )")
pub fn sequence(
  left left: Document,
  items items: List(Document),
  sep sep: Document,
  right right: Document,
  depth depth: Int,
) -> Document {
  let middle =
    items
    |> sequence_help(sep, empty())
  left
  |> append(middle)
  |> group()
  |> indent(depth)
  |> append(right)
}

fn sequence_help(items: List(Document), sep: Document, acc: Document) {
  case items {
    [] -> acc
    [i] -> append(acc, i)
    [i, ..r] -> sequence_help(r, sep, append(acc, append(i, sep)))
  }
}

/// Same as sequence() but has a trailing separator.
/// 
/// ## Examples
/// 
///    let left = append(text("("), break_after(" ", ""))
///    let items =
///      ["one", "two", "three", "four"]
///      |> list.map(text)
///    let sep = break_after(", ", ",")
///    let right = text(")")
///    let seq = sequence_trailing(left, items, sep, right, 2)
///
///    seq
///    |> render(100)
///    |> should.equal("( one, two, three, four, )")
///
///    seq
///    |> render(10)
///    |> should.equal("(
///    one,
///    two,
///    three,
///    four,
///    )")
pub fn sequence_trailing(
  left left: Document,
  items items: List(Document),
  sep sep: Document,
  right right: Document,
  depth depth: Int,
) -> Document {
  let middle =
    items
    |> list.fold(empty(), fn(i, acc) { append(acc, append(i, sep)) })
  left
  |> append(middle)
  |> group()
  |> indent(depth)
  |> append(right)
}

//
// Private Interface
//
//
fn sdoc_to_string(sdoc: SDoc) -> String {
  sdoc_to_string_help(sdoc)
  |> string_builder.to_string()
}

fn sdoc_to_string_help(sdoc: SDoc) -> string_builder.StringBuilder {
  case sdoc {
    SNil -> string_builder.from_string("")
    SText(s, d) -> string_builder.prepend(sdoc_to_string_help(d), s)
    SLine(i, d) -> {
      let prefix = string_builder_repeat(" ", i, string_builder.from_string(""))
      string_builder.prepend(prefix, "\n")
      |> string_builder.append_builder(sdoc_to_string_help(d))
    }
  }
}

fn string_builder_repeat(
  s: String,
  i: Int,
  acc: string_builder.StringBuilder,
) -> string_builder.StringBuilder {
  case i {
    x if x > 0 -> string_builder_repeat(s, i - 1, string_builder.append(acc, s))
    _ -> acc
  }
}

fn fits(width: Int, docs: List(tuple(Int, Mode, Document))) -> Bool {
  case width {
    x if x < 0 -> False
    _ ->
      case docs {
        [] -> True
        [tuple(_, _, Empty), ..r] -> fits(width, r)
        [tuple(i, m, Cons(x, y)), ..r] ->
          fits(width, [tuple(i, m, x), tuple(i, m, y), ..r])
        [tuple(i, m, Nest(j, x)), ..r] -> fits(width, [tuple(i + j, m, x), ..r])
        [tuple(_, _, Text(s)), ..r] -> fits(width - string.length(s), r)
        [tuple(_, Flat, OnBreak(s, _)), ..r] ->
          fits(width - string.length(s), r)
        [tuple(_, Break, OnBreak(_, _)), ..] -> True
        [tuple(i, _, Group(x)), ..r] -> fits(width, [tuple(i, Flat, x), ..r])
      }
  }
}

// w = total line width width, k = how much is aready consumed
fn format(w: Int, k: Int, docs: List(tuple(Int, Mode, Document))) -> SDoc {
  case docs {
    [] -> SNil
    [tuple(_, _, Empty), ..r] -> format(w, k, r)
    [tuple(i, m, Cons(x, y)), ..r] ->
      format(w, k, [tuple(i, m, x), tuple(i, m, y), ..r])
    [tuple(i, m, Nest(j, x)), ..r] -> format(w, k, [tuple(i + j, m, x), ..r])
    [tuple(_, _, Text(s)), ..r] -> SText(s, format(w, k + string.length(s), r))
    [tuple(_, Flat, OnBreak(s, _)), ..r] ->
      SText(s, format(w, k + string.length(s), r))
    [tuple(i, Break, OnBreak(_, pos)), ..r] ->
      case pos {
        Before(s) ->
          case r {
            [] -> SLine(i, format(w, k, [tuple(i, Break, Text(s))]))
            [tuple(i, m, x), ..rr] ->
              SLine(
                i,
                format(w, k, [tuple(i, m, Text(s)), tuple(i, m, x), ..rr]),
              )
          }
        After(s) ->
          case r {
            [] -> SText(s, format(w, k, r))
            [tuple(i, m, x), ..rr] ->
              SText(
                s,
                format(
                  w,
                  k,
                  [
                    tuple(i, Break, OnBreak("", Before(""))),
                    tuple(i, m, x),
                    ..rr
                  ],
                ),
              )
          }
      }
    [tuple(i, _, Group(x)), ..r] ->
      case fits(w - k, [tuple(i, Flat, x), ..r]) {
        True -> format(w, k, [tuple(i, Flat, x), ..r])
        False -> format(w, k, [tuple(i, Break, x), ..r])
      }
  }
}
